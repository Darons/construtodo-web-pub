# Construtodo

Web application for a building company MIS.

**NOTE: Original source code is private, this repository is only intented for demonstration purposes.**

## Technical information

* **Framework**: [Angular](https://angular.io/) v6.1.4
* **UI**: [NG Bootstrap](https://ng-bootstrap.github.io/)
* **Icons**: [Font Awesome](https://fontawesome.com/)
* **Libraries**: 
  * [NGX-Datatable](https://github.com/swimlane/ngx-datatable)
  * [Momentjs](https://momentjs.com/)
  * [RxJS](https://rxjs-dev.firebaseapp.com/)
  * [PDF Make](http://pdfmake.org/)
  * [Sweet Alert](https://sweetalert2.github.io/)

## About my contribution

I developed 14 reports including inventory, financial, managerial and listings reports.

Each report includes a filtering option, a display table (different formats for each type of report), its correspondent services (fetch API data) and exportable PDF (and Excel for some).

I also partipate on the global design (styles), toolbar and sidebar, a combobox component for filters and to organize project structure. 

In this repository you can find an example of report and a general service for PDFs.

![Login Interface](img/4.png?raw=true)  
![Sidebar](img/1.png?raw=true)  
![Filters](img/2.png?raw=true)  
![Report list](img/3.png?raw=true)
