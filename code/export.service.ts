/**
 * Imports
 */

@Injectable()
export class ExportService {
  //?Formato para fecha tipo IN (Dato Date).
  transformDateIn(value: any, args?: any): any {
    if (value != null) {
      let date = value.split("-")
      let year = date[0]
      let month = date[1]
      let day = date[2]
      return day + "/" + month + "/" + year
    } else return null
  }

  //?Formato para fecha tipo OUT (Dato String).
  transformDateOut(value: any, args?: any): any {
    if (value != null) {
      let year = value.substring(0, 4)
      let month = value.substring(4, 6)
      let day = value.substring(6, 8)
      return day + "/" + month + "/" + year
    } else return null
  }

  //?Formato para montos y cantidades.
  transformCurrency(value): string {
    let val = parseFloat(value)
    return new Intl.NumberFormat("es-ve", {
      minimumFractionDigits: 2,
    }).format(val)
  }

  //?Constructor de cuerpo (content) para PDFs (tabla listado).
  /**
   * @param
   *   data: Información que se cargará en la tabla.
   *   key: Array con las key (props) de la tabla.
   *   columns: Array para header (si ya se agregó header en el método enviar un array con strings vacíos).
   * @return
   *   body: Cuerpo de la tabla.
   */
  buildTableBody(data, key, columns) {
    let body = []
    let self = this
    body.push(columns)

    data.forEach((row) => {
      let rowData = []
      key.forEach((key) => {
        if (key == "Fecha" || key == "FechaCheque") {
          rowData.push(self.transformDateOut(row[key]))
        } else if (HEADER_KEYS.includes(key)) {
          rowData.push({
            text: self.transformCurrency(row[key]),
            alignment: "right",
          })
        } else {
          rowData.push(row[key])
        }
      })
      body.push(rowData)
    })
    return body
  }

  //?Suma los montos de una columna.
  /**
   * @params
   *   tipoPago: Array de la columna.
   *   monto: Key de la colummna (prop).
   * @return
   *   Monto acumulado.
   */
  addRowValues(values: any, key: any = "Monto"): Number {
    let added = 0
    values.forEach((element) => {
      added += element[key]
    })
    return added
  }
}
