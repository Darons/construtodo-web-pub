/**
 * Imports
 */

@Injectable()
export class PdfPurchasesVsSales extends ExportService {

  constructor() {
    super();
  }

  //? PDFMake para Compras Vs. Ventas.
  public pdfPurchasesvsSales(filters, values) {
    let self = this;
    let pdf = {
      header: (currentPage, pageCount) => {
        return {
          table: {
            widths: ['20%', '50%', '35%'],
            heights: ['auto', 'auto', 'auto'],
            body: [
              [ //Primera fila
                { //Imagen
                  image: IMAGE,
                  width: 180,
                  height: 100
                },
                { //Membrete
                  text: '\nAv. Los Pinos, entre calle Pomagas y Los Mangos,Casa Nro. 35 Urb. La Florida Norte, Zona Portal 1050 Ofi. +58212731.2087',
                  alignment: 'center',
                  fontSize: 16,
                  margin: [130, 0, 0, 0]
                },
                { //Detalles
                  columns: [
                    [ //Fila
                      {
                        text: '\nFecha de Emisión:  ',
                        bold: 'true'
                      },
                      {
                        text: 'Hora:  ',
                        bold: 'true'
                      },
                      {
                        text: 'Página:  ',
                        bold: 'true'
                      },
                    ],
                    [
                      '\n',
                      self.currentDate[0],
                      self.currentDate[1] + ' ' + self.currentDate[2] + self.currentDate[3],
                      currentPage.toString() + '/' + pageCount.toString()
                    ]
                  ],
                  alignment: 'left',
                  margin: [110, 0, 0, 0]
                }
              ], //Fin Primera Fila
              [ //Segunda fila
                {
                  text: 'TOTAL COMPRAS VS. VENTAS',
                  style: 'header',
                  colSpan: 3,
                  alignment: 'center'
                }, {}, {}
              ], //Fin Segunda Fila
              [ //Tercera fila
                {
                  style: 'tableExample',
                  table: {
                    headerRows: 1,
                    body: [
                      [{
                          text: 'RANGOS',
                          style: 'tableHeader'
                        },
                        {
                          text: 'Artículo:',
                          style: 'tableHeader2'
                        }, ' Desde:', rin.Articulo_D, 'Hasta:', rin.Articulo_h,
                        {
                          text: 'Fecha:',
                          style: 'tableHeader2'
                        }, ' Desde:', self.transformDateIn(rin.Fecha_D), 'Hasta:', self.transformDateIn(rin.Fecha_h),
                      ],
                    ]
                  },
                  colSpan: 3,
                  alignment: 'center',
                  layout: 'noBorders',
                  margin: [240, 0, 0, 0]
                }, {}, {}
              ], //Fin 3ta Fila
              [ //Cuarta fila
                {
                  style: 'tableExample',
                  table: {
                    widths: [150, 210, 55, 75, 67, 62, 60, 62, 60, 70],
                    body: [
                      [{
                          text: 'Artículo',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Descripción',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Unid.',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Stock Ini.',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Compras',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Salidas',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Ventas',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Entradas',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Stock Fin.',
                          style: 'hTable',
                          border: [false, true, false, true],
                        },
                        {
                          text: 'Stock G-01',
                          style: 'hTable',
                          border: [false, true, false, true],
                        }
                      ],
                    ]
                  },
                  colSpan: 3,
                  alignment: 'center',
                  margin: [20, 0, 0, 15]
                }
              ] //Fin 4ta Fila
            ]
          },
          layout: 'noBorders',
          style: 'headerTable'
        }
      },
      content: [{
          style: 'tableExample',
          table: {
            widths: [150, 210, 35, 60, 60, 60, 60, 60, 60, 70],
            body: this.buildTableBody(values.document,
              ['Articulo', 'Descripcion', 'Unidad', 'StockIni', 'Compras', 'Salidas', 'Ventas', 'entradas', 'StockFinal', 'StockG01'],
              ['', '', '', '', '', '', '', '', '', ''])
          },
          layout: 'lightHorizontalLines',
          margin: [10, 0, 0, 0]
        },
        //?Separador
        {
          style: 'tableExample',
          table: {
            widths: [150, 210, 50, 70, 60, 60, 60, 60, 60, 70],
            body: [
              [{
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
                {
                  text: '',
                  border: [false, false, false, true],
                },
              ],
            ],
          },
          margin: [21, 0, 0, 0],
        }, //?Fin de separador
        //?Totales
        {
          style: 'tableExample',
          table: {
            widths: [150, 210, 35, 60, 60, 60, 60, 60, 60, 70],
            body: self.summary1(values),
          },
          layout: 'lightHorizontalLines',
          margin: [10, 0, 0, 0],
        }, //?Fin de totales
        //?Totales artículos que manejan stock
        {
          style: 'tableExample',
          table: {
            style: 'htable',
            widths: [150, 210, 35, 60, 60, 60, 60, 60, 60, 70],
            body: this.summary2(values),
          },
          layout: 'lightHorizontalLines',
          margin: [10, 0, 0, 0],
        }, //?Fin de totales de stock
        //?Totales artículos de servicio
        {
          style: 'tableExample',
          table: {
            widths: [150, 210, 35, 60, 60, 60, 60, 60, 60, 70],
            body: this.summary3(values),
          },
          layout: 'lightHorizontalLines',
          margin: [10, 0, 0, 0],
        }, //?Fin totales de servicio
      ],
      styles: {
        hTable: {
          bold: true,
          alignment: 'left'
        },
        bMTable: {
          alignment: 'right'
        },
        bTable: {
          alignment: 'justify'
        },
        headerTable: {
          margin: [5, 10, 10, 8]
        },

        header: {
          fontSize: 18,
          bold: true,
          alignment: 'justify'
        }
      },
      pageSize: 'LEGAL',
      pageOrientation: 'landscape',
      pageMargins: [10, 200, 40, 10],
      defaultStyle: {
        fontSize: 12
      }
    }
    pdfMake.createPdf(pdf).open();
  }

  summary1(values) {
    let body = [];
    let self = this;
    let row = [];
    row.push('');
    row.push({
      text: 'Totales:',
      bold: true
    });
    row.push('');
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'StockIni')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'Compras')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'Salidas')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'Ventas')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'entradas')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'StockFinal')),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(self.addRowValues(values.document, 'StockG01')),
      alignment: 'right'
    });
    body.push(row);
    return body;
  }

  summary2(values) {
    let body = [];
    let self = this;
    let row = [];
    row.push('');
    row.push({
      text: 'Totales artículos que manejan stock:',
      bold: true
    });
    row.push('');
    row.push({
      text: self.transformCurrency(values.totalStart,
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalPurchase1),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalIn1),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalSale1),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalOut1),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalEnd),
      alignment: 'right'
    });
    row.push('');
    body.push(row);
    return body;
  }

  summary3(values) {
    let body = [];
    let self = this;
    let row = [];
    row.push('');
    row.push({
      text: 'Totales artículos de servicio:',
      bold: true
    });
    row.push('');
    row.push('');
    row.push({
      text: self.transformCurrency(values.totalPurchase2),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalIn2),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalSale2),
      alignment: 'right'
    });
    row.push({
      text: self.transformCurrency(values.totalOut2),
      alignment: 'right'
    });
    row.push('');
    row.push('');
    body.push(row);
    return body;
  }
}
