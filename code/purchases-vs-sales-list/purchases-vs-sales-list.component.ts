/**
 * Imports
 */

@Component({
  selector: 'app-purchases-vs-sales-list',
  templateUrl: './purchases-vs-sales-list.component.html',
  styleUrls: ['./purchases-vs-sales-list.component.css'],
})
export class PurchasesVsSalesComponentList implements OnInit {
  constructor(
    private location: Location,
    public route: ActivatedRoute,
    public pvs: PurchasesVsSalesService,
    public pipe: MonedaConstPipe,
    public pdf: PdfPurchasesVsSalesService,
    public global: Globals
  ) {}

  //?Title.
  title: String

  //?Loader.
  loadingIndicator: boolean = false

  //?Formato de entrada de datos.
  filters: PurchasesVsSalesIn

  //?Listado.
  listOut: PurchasesVsSalesOut
  enableSummary = true
  summaryPosition = 'bottom'
  totals: {}
  rows: []

  //?: Acumuladores para totales.
  addedStart: number
  addedP: number
  addedS: number
  addedI: number
  addedO: number
  addedEnd: number
  addedG01: number

  ngOnInit() {
    //?Inicialización.
    this.title = 'compras vs. ventas'

    //?Recuperar la información enviada por parámetro.
    this.route.queryParams.subscribe((params) => {
      this.filters = params
    })

    this.addedStart = this.addedP = this.addedS = this.addedI = this.addedO = this.addedEnd = this.addedG01 = 0
    this.fillTable(this.filters)
  }

  /*
   *Regresar a la pantalla anterior.
   */
  goBack(): void {
    this.location.back()
  }

  accept() {
    this.global.showLoading()
    this.pdf.pdfPurchasesVsSales(this.filters, this.listOut)
    this.global.hideLoading()
  }

  fillTable(rep: PurchasesVsSalesIn) {
    this.pvs.getPurchasesVsSales(rep).subscribe(
      (res) => {
        this.listOut = res
        this.loadingIndicator = false
        res.data.forEach((a) => {
          this.addedStart = +this.addedStart + +a.start
          this.addedP = +this.addedP + +a.purchases
          this.addedO = +this.addedO + +a.outs
          this.addedS = +this.addedS + +a.sales
          this.addedI = +this.addedI + +a.ins
          this.addedEnd = +this.addedEnd + +a.end
          this.addedG01 = +this.addedG01 + +a.stockG01
        })
        this.rows = res.data

        this.totals = res.totals
        this.loadingIndicator = false
      }, error => this.global.alert(error, 'warning'))
  }
}
