/**
 * Imports
 */

@Injectable()
export class PurchasesVsSalesService extends Headers {
  constructor(public http: HttpClient) {
    super()
  }

  url = environment.API + "" //endpoint hidden

  getPurchasesVsSales (filters: PurchasesVsSalesIn): Observable<PurchasesVsSalesOut> {
    return this.http.post<PurchasesVsSalesOut> (this.url, filters,
      {
        headers: this.headers,
      }
    )
  }
}
