/**
 * Imports
 */

import { PurchasesVsSalesService } from "../purchases-vs-sales.service"

const now = new Date()
@Component({
  selector: 'app-purchases-vs-sales',
  templateUrl: './purchases-vs-sales.component.html',
  styleUrls: ['./purchases-vs-sales.component.css'],
})
export class PurchasesVsSalesComponent implements OnInit {
  constructor(
    public lisSto: StockService,
    public lisLin: LineService,
    public lisCat: CateogoryService,
    public lisPro: ProviderService,
    public lisMov: MovementsService,
    public lisUni: UnitTypeService,
    public router: Router,
    public pdf: PdfPurchasesVsSalesService,
    public global: Globals,
    public pvs: PurchasesVsSalesService
  ) {}

  //?Title.
  title: String

  //?Requests callbacks.
  storage: Observable<any>
  line: Observable<any>
  category: Observable<any>
  provider: Observable<any>
  movements: Observable<any>
  unitType: Observable<any>

  //?Formgroup.
  filtersForm: FormGroup

  //?Models.
  send = ''
  filters: PurchasesVsSalesFilters

  //?Dates.
  datF: NgbDateStruct
  datU: NgbDateStruct
  maxDate: NgbDateStruct

  //?Boolean flag para campo de artículo.
  flag: boolean = true

  ngOnInit() {
    this.title = 'Compras vs. ventas'
    this.clearFields()

    //?Creación del FormGroup.
    this.filtersForm = new FormGroup({
      itemF: new FormControl(''),
      itemU: new FormControl(''),
      dateF: new FormControl('', [Validators.required]),
      dateU: new FormControl('', [Validators.required]),
      storageF: new FormControl(''),
      storageU: new FormControl(''),
      lineF: new FormControl(''),
      lineU: new FormControl(''),
      categoryF: new FormControl(''),
      categoryU: new FormControl(''),
      providerF: new FormControl(''),
      providerU: new FormControl(''),
      movements: new FormControl(''),
      unitType: new FormControl(''),
      sendTo: new FormControl('', [Validators.required]),
    })

    //?Fill combobox filters
    this.storage = this.lisSto.getStocksList()
    this.line = this.lisLin.getLinesList()
    this.provider = this.lisPro.getProvidersList()
    this.category = this.lisCat.getCategoriesList()
    this.movements = this.lisMov.getMovementsList()
    this.unitType = this.lisUni.getUnitTypesList()
  }

  /*
   *Reiniciar los campos.
   */
  clearFields() {
    this.send = ''
    this.datU = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate(),
    }
    this.maxDate = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate(),
    }
    this.datF = {
      year: now.getFullYear(),
      month: 1,
      day: 1,
    }
  }

  /*
   *Regresar a la pantalla anterior.
   */
  goBack(): void {
    this.router.navigate([`app/reportes/dashboard`])
  }

  /*
   *Se validan los campos del formulario y se envían los datos de filtrado a la siguiente vista, o al servicio de PDF.
   */
  accept() {
    let a = this.filtersForm.value;
    let dF = a.dateF.year.toString() + '-' + a.dateF.month.toString() + '-' + a.dateF.day.toString();
    let dU = a.dateU.year.toString() + '-' + a.dateU.month.toString() + '-' + a.dateU.day.toString();

    if (this.filtersForm.valid) {
      if (a.sendTo == 'Listado') {
        this.flag = true;
        this.filtersForm.controls.dateF.setValue(dF);
        this.filtersForm.controls.dateU.setValue(dU);
        this.router.navigate([`${this.router.url}/detalle`], {
          queryParams: this.filtersForm.value
        });
      } else if (a.sendTo == 'PDF') {
        this.global.showLoading()
        this.pvs.getPurchasesVsSales(this.filtersForm.value).subscribe(res => {
            this.global.hideLoading()
            this.pdf.pdfPurchasesVsSales(this.filtersForm.value, res);
          },
          error => this.global.alert(error, 'warning'))
      }
    } else this.global.alert('Debe seleccionar una opción válida.', 'info')
  }

  /*
   *Registra los cambios en los combos.
   */
  changeStorage(code: String) {
    this.filters.stoU = code
  }
  changeLine(code: String) {
    this.filters.linU = code
  }
  changeCategory(code: String) {
    this.filters.cateU = code
  }
  changeProvider(code: String) {
    this.filters.provU = code
  }
}
